import React from 'react';

const ContentTitle = ({firstname, lastname}) => {
  return (
    <h1>
      {firstname} {lastname}
    </h1>
  );
};

export default ContentTitle;
