import React from 'react';
// import ContentList from 'ContentList';
import ContentData from './ContentData';
import ContentTitle from './ContentTitle';
import ContentImage from './ContentImage';

// import PropTypes from 'prop-types';

const App = ({data}) => {
  return (
    <div classProfile="content">
      <ContentImage image={data.picture.large} />
      <ContentTitle lastname={data.name.last} firstname={data.name.first} />
      <ContentData
        userName={data.login.username}
        mail={data.email}
        phone={data.phone}
        age={data.dob.age}
      />
    </div>
  );
};

export default App;
