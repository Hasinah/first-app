import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/ContentList';
// ReactDOM.render(<App />, document.getElementById("root"));//c'est le même que en bas
const AppElement = document.getElementById('root');
ReactDOM.render(<App />, AppElement);
module.hot.accept();
