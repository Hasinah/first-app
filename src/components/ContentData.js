import React from 'react';

const ContentData = ({userName, age, mail, phone}) => {
  const handleClick = (e) => {
    document.getElementsByClassName('profile')[0].style.textDecoration =
      'line-through';
  };

  const handleClick2 = () => {
    document.getElementsByClassName('profile3')[0].style.textDecoration =
      'line-through';
  };
  const handleClick3 = () => {
    document.getElementsByClassName('profile4')[0].style.textDecoration =
      'line-through';
  };
  const handleClick1 = () => {
    document.getElementsByClassName('profile2')[0].style.textDecoration =
      'line-through';
  };
  return (
    <ul>
      <li className="profile" onClick={handleClick}>
        Nom d'identifant: {userName}
      </li>
      <li className="profile3" onClick={handleClick2}>
        Email: {mail}
      </li>
      <li className="profile4" onClick={handleClick3}>
        Tél: {phone}
      </li>
      <li className="profile2" onClick={handleClick1}>
        Age: {age}
      </li>
    </ul>
  );
};

export default ContentData;
