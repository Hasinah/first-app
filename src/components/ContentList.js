import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import App from './App';

const ReactRoot = document.getElementById('root');
axios
  .get('https://randomuser.me/api/') // cherche l'api
  .then((response) => {
    const {data} = response;
    console.log(data.results[0]); //affiche le tableaux
    ReactDOM.render(<App data={data.results[0]} />, ReactRoot);
  })
  .catch((error) => console.error(error)); // affiche error si il ya errore

export default ReactRoot;
