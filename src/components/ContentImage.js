import React from 'react';
const ContentImage = ({image}) => {
  return (
    <div classProfile="profileImage">
      <img src={image} alt=""></img>
    </div>
  );
};

export default ContentImage;
